package com.example.weather.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weather.R;
import com.example.weather.model.item;
import com.example.weather.model.item_hour;
import com.squareup.picasso.Picasso;

import java.util.List;

public class itemhouradapter extends RecyclerView.Adapter<itemhouradapter.myviewholder> {

    Context context ;
    List<item_hour> data ;

    public itemhouradapter(Context context, List<item_hour> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public myviewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_hourly,parent,false);
        return new myviewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull myviewholder holder, int position) {


        holder.timenow.setText(data.get(position).getTimeNow());
        holder.tempnow.setText(data.get(position).getTempNow());
        Picasso.get().load(data.get(position).getImage()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myviewholder extends  RecyclerView.ViewHolder {
        TextView timenow , tempnow ;
        ImageView imageView;

        public myviewholder(@NonNull View itemView) {
            super(itemView);
            timenow = itemView.findViewById(R.id.tv_hourly_time_now);
            tempnow = itemView.findViewById(R.id.tv_temp_now_hourly);
            imageView = itemView.findViewById(R.id.iv_hourly);


        }
    }
}
