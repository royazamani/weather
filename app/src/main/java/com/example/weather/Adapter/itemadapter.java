package com.example.weather.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weather.R;
import com.example.weather.model.item;
import com.squareup.picasso.Picasso;

import java.util.List;

public class itemadapter extends RecyclerView.Adapter<itemadapter.myviewholder> {

    Context context ;
    List<item> data ;

    public itemadapter(Context context, List<item> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public myviewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item,parent,false);
        return new myviewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull myviewholder holder, int position) {


        holder.title.setText(data.get(position).getTitle());
        holder.max.setText(data.get(position).getTemp_max());
        holder.min.setText(data.get(position).getTemp_min());
        Picasso.get().load(data.get(position).getImage()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myviewholder extends  RecyclerView.ViewHolder {
        TextView title , max , min ;
        ImageView imageView;

        public myviewholder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tv_friday);
            max = itemView.findViewById(R.id.tv_friday_max);
            min = itemView.findViewById(R.id.tv_friday_min);
            imageView = itemView.findViewById(R.id.iv_friday);


        }
    }
}
