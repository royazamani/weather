package com.example.weather.Global;

import android.content.Context;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.weather.Adapter.IntroAdapter;
import com.example.weather.Adapter.itemadapter;
import com.example.weather.MainActivity;
import com.example.weather.MainActivity2;
import com.example.weather.model.Intro;
import com.example.weather.model.item;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Global {

    static final String link = "http://192.168.43.126:80/weather/intro.php";
    RequestQueue requestQueue;
    List<Intro> data = new ArrayList<>();
    IntroAdapter introAdapter;

    //daily
    DecimalFormat format = new DecimalFormat("#.##");

    final String url_image = "http://openweathermap.org/img/w/";
    List<item> itemList = new ArrayList<>();
    itemadapter itemadapter;
    String link_daily = "http://api.openweathermap.org/data/2.5/onecall?lat=";

    public void getIntro(final Context context, RequestQueue requestQueue, String url, final ViewPager viewPager, final List<Intro> intros) {
        this.requestQueue = requestQueue;
        requestQueue = Volley.newRequestQueue(context);
        String FinalLink = link + url;
        this.data = intros;
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, FinalLink
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("weather_app");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        String id = jsonObject.getString("id");
                        String link_img = jsonObject.getString("link_img");
                        String description = jsonObject.getString("description");


                        Intro intro = new Intro();
                        intro.setId(id);
                        intro.setLink_img(link_img);
                        intro.setDecsription(description);


                        data.add(intro);
                        introAdapter = new IntroAdapter(context, data);
                        viewPager.setAdapter(introAdapter);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(jsonObjectRequest);

    }
}

