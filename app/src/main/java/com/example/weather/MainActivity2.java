package com.example.weather;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.weather.Adapter.itemadapter;
import com.example.weather.Adapter.itemhouradapter;
import com.example.weather.Global.Global;
import com.example.weather.model.item;
import com.example.weather.model.item_hour;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Struct;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.security.auth.Subject;

public class MainActivity2 extends AppCompatActivity {
    RequestQueue requestQueue;
    final String API_KEY = "116db00ca1a0b5231d474a1a73b655fc";
    final String url_image = "http://openweathermap.org/img/w/";
    Bundle bundle;
    TextView textViewname;
    DecimalFormat format = new DecimalFormat("#");

    // daily
    RecyclerView recyclerView_daily;
    final String url = "https://api.openweathermap.org/data/2.5/onecall?lat=";
    List<item> itemList = new ArrayList<>();
    itemadapter itemadapter;


    // hourly
    RecyclerView recyclerView_hourly;
    List<item_hour> item_hours = new ArrayList<>();
    itemhouradapter itemhouradapter;

    //http://api.openweathermap.org/data/2.5/onecall?lat=38.08&lon=46.2919&exclude=current,minutely,hourly,alerts&appid=116db00ca1a0b5231d474a1a73b655fc

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        requestQueue = Volley.newRequestQueue(this);
        bundle = getIntent().getExtras();

        String lat = bundle.getString("lat");
        String lon = bundle.getString("lon");
        String name= bundle.getString("name");

        textViewname = findViewById(R.id.tv_ciryName_2);
        textViewname.setText(name);
         AlphaAnimation alphaAnimation = new AlphaAnimation(1, 0);
        alphaAnimation.setRepeatMode(Animation.REVERSE);
        alphaAnimation.setRepeatCount(Animation.INFINITE);
        alphaAnimation.setDuration(1500);
        textViewname.setAnimation(alphaAnimation);


        //daily
        final StringRequest stringRequest = new StringRequest(Request.Method.GET, url + lat + "&lon=" + lon + "&exclude=current,minutely,hourly,alerts&appid=" + API_KEY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONObject object = new JSONObject(response);

                    JSONArray daily = object.getJSONArray("daily");
                    for (int i =0; i < daily.length()-1; i++) {
                        JSONObject json = daily.getJSONObject(i);
                        item item = new item();
                        long dt = json.getLong("dt");
                        java.util.Date time=new java.util.Date(dt*1000);
                        String date = time.toString().substring(0, 4);
                        item.setTitle(date + "");

                        JSONObject temp = json.getJSONObject("temp");

                        double min = temp.getDouble("min") - 273.15;
                        item.setTemp_min(format.format(min) + "");
                        double max = temp.getDouble("max") - 273.15;
                        item.setTemp_max(format.format(max) + "");


                        JSONArray weather = json.getJSONArray("weather");
                        JSONObject jsonObjectweather = weather.getJSONObject(0);
                        String icon = jsonObjectweather.getString("icon");
                        item.setImage(url_image + icon + ".png");



                        itemList.add(item);

                    }
                    recyclerView_daily = findViewById(R.id.rv_daily);
                    recyclerView_daily.setHasFixedSize(true);
                    recyclerView_daily.setLayoutManager(new LinearLayoutManager(MainActivity2.this, RecyclerView.VERTICAL, false));
                    itemadapter = new itemadapter(getApplicationContext(), itemList);
                    recyclerView_daily.setAdapter(itemadapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(MainActivity2.this , e.getMessage(),Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity2.this , error.getMessage(),Toast.LENGTH_LONG).show();

            }
        });
        requestQueue.add(stringRequest);

        ////////////////
        final StringRequest stringRequest1 = new StringRequest(Request.Method.GET, url + lat + "&lon=" + lon + "&exclude=current,minutely,daily,alerts&appid=" + API_KEY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONObject object = new JSONObject(response);

                    JSONArray hourly = object.getJSONArray("hourly");
                    for (int i =0; i < hourly.length(); i++) {
                        JSONObject json = hourly.getJSONObject(i);
                        item_hour item = new item_hour();
                        long dt = json.getLong("dt");
                        java.util.Date time=new java.util.Date(dt*1000);
                        String date = time.toString().substring(10, 13);
                        item.setTimeNow(date + "");

                        double temp = json.getDouble("temp")-273.15;
                        item.setTempNow(format.format(temp)+"°");


                        JSONArray weather = json.getJSONArray("weather");
                        JSONObject jsonObjectweather = weather.getJSONObject(0);
                        String icon = jsonObjectweather.getString("icon");
                        item.setImage(url_image + icon + ".png");



                        item_hours.add(item);

                    }
                    recyclerView_hourly = findViewById(R.id.rv_hourly);
                    recyclerView_hourly.setHasFixedSize(true);
                    recyclerView_hourly.setLayoutManager(new LinearLayoutManager(MainActivity2.this, RecyclerView.HORIZONTAL, false));
                    itemhouradapter = new itemhouradapter(getApplicationContext(), item_hours);
                    recyclerView_hourly.setAdapter(itemhouradapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(MainActivity2.this , e.getMessage(),Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity2.this , error.getMessage(),Toast.LENGTH_LONG).show();

            }
        });
        requestQueue.add(stringRequest1);

    }
}