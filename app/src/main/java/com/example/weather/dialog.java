package com.example.weather;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.text.Layout;
import android.text.style.BulletSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AlertDialogLayout;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.textfield.TextInputEditText;

public class dialog extends DialogFragment {
    private dialogeventlistener dialogeventlistener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        dialogeventlistener = (dialog.dialogeventlistener) context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog,null,false);
        builder.setView(view);

        Button btn_ok = view.findViewById(R.id.btn_ok);
        Button btn_cancel = view.findViewById(R.id.btn_cancel);
        final EditText editText = view.findViewById(R.id.et_weather);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (editText.length()>0){

                    dialogeventlistener.onOkBtnClickListener(editText.getText().toString());
                    dismiss();

                }else{
                    editText.setError("please enter your city");
                }
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogeventlistener.onCancelBtnClickListener();
                dismiss();
            }
        });
        return builder.create();

    }

    public interface dialogeventlistener {
    void onOkBtnClickListener(String data);
    void onCancelBtnClickListener();
    }
}
