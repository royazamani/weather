package com.example.weather;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.weather.Adapter.IntroAdapter;
import com.example.weather.Global.Global;
import com.example.weather.model.Intro;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class IntroActivity extends AppCompatActivity {

    RequestQueue requestQueue ;
    ViewPager viewPager;
    TabLayout tabLayout;
    Global global = new Global();
    List<Intro> introList = new ArrayList<>();
    IntroAdapter introAdapter;
    Button btn_gotoapp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        requestQueue = Volley.newRequestQueue(this);
        viewPager = findViewById(R.id.viewpagerintro);
        btn_gotoapp = findViewById(R.id.btn_gotoapp);
        tabLayout = findViewById(R.id.tablayoutintro);
        introAdapter = new IntroAdapter(getApplicationContext(), introList);
        global.getIntro(getApplicationContext(), requestQueue, "", viewPager, introList);
        viewPager.setAdapter(introAdapter);
        tabLayout.setupWithViewPager(viewPager, true);
        btn_gotoapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(IntroActivity.this , MainActivity.class));
                finish();

            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == introList.size() - 1) {
                    btn_gotoapp.setVisibility(View.VISIBLE);
                    viewPager.setVisibility(View.INVISIBLE);
                } else {
                    btn_gotoapp.setVisibility(View.INVISIBLE);
                    viewPager.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}