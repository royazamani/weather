package com.example.weather;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;

public class SplashActivity extends AppCompatActivity {

    LottieAnimationView lottieAnimationView ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        lottieAnimationView = findViewById(R.id.lottie_splash_activity);


        CheckConnection();
    }

    private void CheckConnection() {
        Handler handler = new Handler();
        if (ConnectionManager.checkInternetConnection(getApplicationContext())){
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    startActivity(new Intent(SplashActivity.this,IntroActivity.class));
                    finish();

                }
            }, 4000);

        }else{

            Toast.makeText(SplashActivity.this,"Please Check Your Internet Connection",Toast.LENGTH_LONG).show();

        }
    }
}