package com.example.weather;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AlertDialogLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.function.DoubleUnaryOperator;

public class MainActivity extends AppCompatActivity implements dialog.dialogeventlistener {


    RequestQueue requestQueue;
    final String url = "https://api.openweathermap.org/data/2.5/weather?q=";
    final String API_KEY = "116db00ca1a0b5231d474a1a73b655fc";
    final String url_image = "http://openweathermap.org/img/w/";

    //http://api.openweathermap.org/data/2.5/weather?q=tabriz&appid=116db00ca1a0b5231d474a1a73b655fc

    TextView tv_weather, tv_name_city_main, tv_temp_main;
    RelativeLayout relativeLayout;
    ImageView imageView, iv_search, imageViewnext;

    String city_name, lat, lon;
    DecimalFormat format = new DecimalFormat("#.##");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestQueue = Volley.newRequestQueue(this);

        relativeLayout = findViewById(R.id.rl_main);
        imageView = findViewById(R.id.iv_main_icon);
        imageViewnext = findViewById(R.id.imageView_next);
        tv_weather = findViewById(R.id.tv_weather);
        tv_name_city_main = findViewById(R.id.tv_name_city_main);
        tv_temp_main = findViewById(R.id.tv_temp_main);
        iv_search = findViewById(R.id.iv_search_main);
        tv_weather.setVisibility(View.GONE);
        relativeLayout.setVisibility(View.GONE);
        imageView.setVisibility(View.GONE);
        imageViewnext.setVisibility(View.GONE);

        iv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog dialog = new dialog();
                dialog.setCancelable(false);
                dialog.show(getSupportFragmentManager(), null);
            }
        });

    }

    @Override
    public void onOkBtnClickListener(String data) {
        city_name = data;

        final AlphaAnimation alphaAnimation = new AlphaAnimation(1, 0);
        alphaAnimation.setRepeatMode(Animation.REVERSE);
        alphaAnimation.setRepeatCount(Animation.INFINITE);
        alphaAnimation.setDuration(1500);

        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url + city_name + "&appid=" + API_KEY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                relativeLayout.setVisibility(View.VISIBLE);
                tv_weather.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.VISIBLE);
                imageViewnext.setVisibility(View.VISIBLE);
                imageViewnext.startAnimation(alphaAnimation);

                String result = "";
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("weather");
                    JSONObject jsonObjectweather = jsonArray.getJSONObject(0);
                    String description = jsonObjectweather.getString("description");
                    String icon = jsonObjectweather.getString("icon");
                    JSONObject jsonObjectmain = jsonObject.getJSONObject("main");
                    double temp = jsonObjectmain.getDouble("temp") - 273.15;
                    double feels_like = jsonObjectmain.getDouble("feels_like");
                    float pressure = jsonObjectmain.getInt("pressure");
                    int humidity = jsonObjectmain.getInt("humidity");

                    JSONObject jsonObjectwind = jsonObject.getJSONObject("wind");
                    String wind = jsonObjectwind.getString("speed");

                    JSONObject jsonObjectcoord = jsonObject.getJSONObject("coord");
                    lon = jsonObjectcoord.getString("lon");
                    lat = jsonObjectcoord.getString("lat");

                    JSONObject jsonObjectclouds = jsonObject.getJSONObject("clouds");
                    String clouds = jsonObjectclouds.getString("all");

                    JSONObject jsonObjectsys = jsonObject.getJSONObject("sys");
                    String countryName = jsonObjectsys.getString("country");
                    String cityName = jsonObject.getString("name");

                    result = "weather of " + cityName + ":" +
                            "\n temp: " + format.format(temp) + " C" +
                            "\n feels like: " + format.format(feels_like) + " C" +
                            "\n humidity: " + humidity + " %" +
                            "\n wind speed: " + wind +
                            "\n pressure: " + pressure + " Pa" +
                            "\n description: " + description;
                    ;


                    tv_weather.setText(result);
                    tv_name_city_main.setText(cityName);
                    tv_temp_main.setText(format.format(temp) + " C");
                    Picasso.get().load(url_image + icon + ".png").into(imageView);


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_LONG).show();

            }
        });
        requestQueue.add(stringRequest);

        imageViewnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                intent.putExtra("lat", lat);
                intent.putExtra("lon", lon);
                intent.putExtra("name",city_name);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onCancelBtnClickListener() {

    }
}